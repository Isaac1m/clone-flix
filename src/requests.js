const API_KEY = "ba9f1589811fc8ec4429f4f5beb02d25";
// const API_VERSION = "3";

const endpoints = {
  getTrending: `/trending/all/week?api_key=${API_KEY}`,
  getNetflixOriginals: `/discover/tv?api_key=${API_KEY}&with_networks=213`,
  getTopRated: `/movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`,
  getActionMovies: `/discover/movie?api_key=${API_KEY}&with_genres=28`,
  getComedyMovies: `/discover/movie?api_key=${API_KEY}&with_genres=35`,
  getThrillerMovies: `/discover/movie?api_key=${API_KEY}&with_genres=53`,
  getDocumentaries: `/discover/movie?api_key=${API_KEY}&with_genres=99`,
  getRomanceMovies: `/discover/movie?api_key=${API_KEY}&with_genres=10749`,
};

export default endpoints;
