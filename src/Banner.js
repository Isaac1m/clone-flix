import React, { useState, useEffect } from "react";
import axiosHelper from "./axios";
import requests from "./requests";
import "./Banner.css";

//
function Banner() {
  const [movie, setMovie] = useState([]);
  const baseUrl = `https://image.tmdb.org/t/p/original/`;

  useEffect(() => {
    async function getData() {
      const res = await axiosHelper.get(requests.getTrending);
      setMovie(
        res.data.results[
          Math.floor(Math.random() * res.data.results.length - 1)
        ]
      );
    }

    getData();
  }, []);

  function truncate(str, n) {
    return str?.length > n ? str?.substr(0, n - 1) + "..." : str;
  }
  return (
    <header
      className="banner"
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(${baseUrl}${movie?.backdrop_path})`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner__content">
        {/* BG image */}
        {/* Title */}
        <h1 className="banner__title">
          {movie?.title || movie?.name || movie?.original_name}
        </h1>
        {/* Buttons */}
        <div className="banner__buttons">
          <button className="banner__button">Play</button>
          <button className="banner__button">My List</button>
        </div>
        {/* Desc */}

        <h1 className="banner__description">
          {truncate(movie?.overview, 200)}
        </h1>
      </div>
      <div className="banner--fadeBottom" />
    </header>
  );
}

export default Banner;
