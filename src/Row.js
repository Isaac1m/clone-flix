import React, { useState, useEffect } from "react";
import axiosHelper from "./axios";
import YouTube from "react-youtube";
import "./Row.css";
import movieTrailer from "movie-trailer";

function Row({ title, url, isLargePoster }) {
  const baseUrl = `https://image.tmdb.org/t/p/original`;

  const [movies, setMovies] = useState([]);
  const [trailerUrl, setTrailerUrl] = useState("");

  useEffect(() => {
    async function fetchMovies() {
      const res = await axiosHelper.get(url);
      // console.table(res.data.results);
      setMovies(res.data.results);
    }
    fetchMovies();
  }, [url]);

  const opts = {
    height: "390",
    width: "100%",
    playerVars: {
      autoplay: 1,
    },
  };

  const handleClick = (movie) => {
    if (trailerUrl) {
      setTrailerUrl("");
    } else {
      movieTrailer(movie?.name || movie?.title || movie?.original_name || "")
        .then((url) => {
          const urlParams = new URLSearchParams(new URL(url).search);
          setTrailerUrl(urlParams.get("v"));
        })
        .catch((err) => console.error(err));
    }
  };

  return (
    <div className="row">
      <h2 className="row__title">{title}</h2>
      <div className="row__posters">
        {movies.map((movie) => (
          <img
            key={movie.id}
            onClick={() => handleClick(movie)}
            className={`row__poster ${isLargePoster && "large__poster"}`}
            src={`${baseUrl}${
              isLargePoster ? movie.poster_path : movie.backdrop_path
            }`}
            alt={movie?.title || movie?.name || movie?.original_name}
          />
        ))}
      </div>
      {trailerUrl && <YouTube videoId={trailerUrl} opts={opts} />}
    </div>
  );
}

export default Row;
