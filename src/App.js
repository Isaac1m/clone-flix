import "./App.css";
import Row from "./Row";
import endpoints from "./requests";
import Banner from "./Banner";
import Nav from "./Nav";
function App() {
  return (
    <div className="App">
      {/* Navbar */}
      <Nav />
      {/* Banner */}
      <Banner />
      <Row
        title="Netflix Originals"
        url={endpoints.getNetflixOriginals}
        isLargePoster
      />
      <Row title="Trending Now" url={endpoints.getTrending} />
      <Row title="Action Movies" url={endpoints.getActionMovies} />
      <Row title="Comedy Movies" url={endpoints.getComedyMovies} />
      <Row title="Thriller Movies" url={endpoints.getThrillerMovies} />
      <Row title="Romance Movies" url={endpoints.getRomanceMovies} />
      <Row title="Documentaries" url={endpoints.getDocumentaries} />
      <Row title="Top Rated Movies" url={endpoints.getTopRated} />
    </div>
  );
}

export default App;
